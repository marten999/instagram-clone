<?php namespace App\Repositories;

class PostRepository extends Repository {

    /**
     * Specify Model class name
     *
     * @return mixed
     */
    function model()
    {
        return 'App\Models\Post';
    }

}
