<?php

namespace App\Repositories;

use Illuminate\Container\Container as App;
use Illuminate\Database\Eloquent\Model;

/**
 * Class Repository
 * @package Bosnadev\Repositories\Eloquent
 */
abstract class Repository implements RepositoryInterface
{

    /**
     * @var App
     */

    /**
     * @var
     */
    protected $model;

    /**
     * @param App $app
     * @throws \Bosnadev\Repositories\Exceptions\RepositoryException
     */
    public function __construct()
    {
        $this->makeModel();
    }

    /**
     * Specify Model class name
     *
     * @return mixed
     */
    abstract public function model();

    /**
     * @param array $columns
     * @return mixed
     */
    public function all($columns = ['*'])
    {
        return $this->model->get($columns);
    }

    /**
     * @param array $columns
     * @return mixed
     */
    public function get($columns = ['*'])
    {
        return $this->model->get($columns);
    }

    /**
     * @param array $columns
     * @return mixed
     */
    public function first()
    {
        return $this->model->first();
    }



    /**
     * @param int $perPage
     * @param array $columns
     * @return mixed
     */
    public function paginate($perPage = 15, $columns = ['*'])
    {
        $this->model = $this->model->paginate($perPage, $columns);
        return $this;
    }

    /**
     * @param array $data
     * @return mixed
     */
    public function create(array $data)
    {


        $model = $this->model->create($data);

        // log

        $connection = \Illuminate\Support\Facades\DB::connection()->getName();

        //don't log system connection due to  migration/seeding errors for system db

        if ($connection != "system") {
            $this->logEvent('create', $data, $model->getPrimaryKey());
        }


        return $model;
    }

    /**
     * @param array $data
     * @param $id
     * @param string $attribute
     * @return mixed
     */
    public function update(array $data, $id, $attribute = null)
    {

        if ($attribute == null)
            $attribute = $this->model->getPrimaryKeyName();

        return $this->model->where($attribute, '=', $id)->update($data);
    }

    /**
     * @param $id
     * @return mixed
     */
    public function delete($id)
    {
        return $this->model->destroy($id);
    }

    /**
     * @param $id
     * @param array $columns
     * @return mixed
     */
    public function find($id, $columns = ['*'])
    {
        return $this->model->find($id, $columns);
    }

    /**
     * @param $attribute
     * @param $value
     * @param array $columns
     * @return mixed
     */
    public function findBy($attribute, $value, $columns = ['*'])
    {
        return $this->model->where($attribute, '=', $value)->first($columns);
    }


    /**
     * search
     * @param  [type] $query [description]
     * @return [type]        [description]
     */
    public function search($query)
    {
        $this->model = $this->model->whereLike('name', $query);
        return $this;
    }

    /**
     * order by
     * @param  [type] $col [description]
     * @param  [type] $dir [description]
     * @return [type]      [description]
     */
    public function orderBy($col, $dir)
    {
        $this->model = $this->model->orderby($col, $dir);
        return $this;
    }

    /**
     * where
     * @param  [type] $a [description]
     * @param  [type] $b [description]
     * @param  [type] $c [description]
     * @return [type]    [description]
     */
    public function where($a, $b, $c = null)
    {
        $this->model = $this->model->where($a, $b, $c);
        return $this;
    }

    public function whereIn($a, $b)
    {
        $this->model = $this->model->whereIn($a, $b);
        return $this;
    }

    /**
     * [limit description]
     * @param  [type] $limit [description]
     * @return [type]        [description]
     */
    public function limit($limit)
    {
        $this->model = $this->model->limit($limit);
        return $this;
    }


    /**
     * where
     * @param  [type] $a [description]
     * @param  [type] $b [description]
     * @param  [type] $c [description]
     * @return [type]    [description]
     */
    public function whereTranslation($a, $b, $c = null)
    {
        $this->model = $this->model->whereTranslation($a, $b, $c);
        return $this;
    }



    /**
     * where
     * @param  [type] $a [description]
     * @param  [type] $b [description]
     * @param  [type] $c [description]
     * @return [type]    [description]
     */
    public function whereHas($a, $b)
    {
        $this->model = $this->model->whereHas($a, $b);
        return $this;
    }



    /**
     * return count
     * @return [type] [description]
     */
    public function count()
    {
        return $this->model->count();
    }

    /**
     * active
     * @param  integer $active [description]
     * @return [type]          [description]
     */
    public function active($active = 1)
    {
        $this->where('active', $active);
        return $this;
    }

    /**
     * @return \Illuminate\Database\Eloquent\Builder
     * @throws RepositoryException
     */
    public function makeModel()
    {
        $modelName = $this->model();
        $model = new $modelName;

        if (!$model instanceof Model) {
            throw new \Exception("Class {$this->model()} must be an instance of Illuminate\\Database\\Eloquent\\Model");
        }

        return $this->model = $model;
    }


    public function getCount($from = null, $to = null)
    {

        $q = $this->model;
        if ($from != null)
            $q = $q->where('created_at', '>=', $from);
        if ($to != null)
            $q = $q->where('created_at', '<=', $to);
        return $q->count();
    }


    public function newestQuery($query)
    {
        return $query->orderBy('created_at', 'DESC');
    }


    public function allNewest($columns = ['*'])
    {
        return $this->model->orderBy('created_at', 'DESC')->get($columns);
    }
}
