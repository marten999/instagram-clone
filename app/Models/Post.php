<?php

namespace App\Models;

use Actuallymab\LaravelComment\Contracts\Commentable;
use Actuallymab\LaravelComment\HasComments;
use Cog\Contracts\Love\Reactable\Models\Reactable as ReactableInterface;
use Cog\Laravel\Love\Reactable\Models\Traits\Reactable;use Illuminate\Database\Eloquent\Model;
use Spatie\Tags\HasTags;

class Post extends Model implements ReactableInterface, Commentable
{
    use HasTags, Reactable, HasComments;

    protected $table      = 'posts';
	protected $primaryKey = 'post_id';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_id',
        'description',
    ];


	// Relations
	public function user()
    {
        return $this->hasOne('App\Models\User', 'user_id', 'user_id');
    }

    
    // Getters
	public function getPostID() {
		return $this->primaryKey;
	}
	public function getUserID() {
		return $this->user_id;
	}
	public function getDescription() {
		return $this->description;
	}

}
