<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Relations\Pivot;

class UserFollower extends Pivot
{

    protected $table      = 'user_followers';
	protected $primaryKey = 'user_follower_id';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_id',
        'follower_id',
    ];

	// Relations
	public function user()
    {
        return $this->hasOne('App\Models\User', 'user_id', 'user_id');
    }
	public function follower()
    {
        return $this->hasOne('App\Models\User', 'user_id', 'follower_id');
    }
    
    // Getters
	public function getUserFollowerID() {
		return $this->primaryKey;
	}
	public function getUserID() {
		return $this->user_id;
	}
	public function getFollowerID() {
		return $this->follower_id;
	}

}
