<?php

namespace App\Models;

use Actuallymab\LaravelComment\CanComment;
use Cog\Contracts\Love\Reacterable\Models\Reacterable as ReacterableInterface;
use Cog\Laravel\Love\Reacterable\Models\Traits\Reacterable;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Tymon\JWTAuth\Contracts\JWTSubject;

class User extends Authenticatable implements ReacterableInterface, JWTSubject
{
    use HasFactory, Notifiable, Reacterable, CanComment;

    protected $table      = 'users';
	protected $primaryKey = 'user_id';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'surname',
        'username',
        'email',
        'password',
        'bio',
        'is_private',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];


	// Relations
	public function profileFile()
    {
        return $this->hasOne('App\Models\File', 'user_id', 'user_id');
    }

    
    // Getters
     /**
     * Get the identifier that will be stored in the subject claim of the JWT.
     *
     * @return mixed
     */
    public function getJWTIdentifier()
    {
        return $this->getKey();
    }
    /**
     * Return a key value array, containing any custom claims to be added to the JWT.
     *
     * @return array
     */
    public function getJWTCustomClaims()
    {
        return [];
    }
	public function getUserID() {
		return $this->primaryKey;
	}
	public function getName() {
		return $this->name;
	}
	public function getSurname() {
		return $this->surname;
	}
	public function getUsername() {
		return $this->username;
	}
	public function getEmail() {
		return $this->email;
	}
	public function getPassword() {
		return $this->password;
	}
	public function getBio() {
		return $this->bio;
	}
	public function getIsPrivate() {
		return $this->is_private;
    }
    

    public function isPrivate() {
        if ($this->getIsPrivate() == 1) {
            return true;
        } 
        return false;
	}
}
