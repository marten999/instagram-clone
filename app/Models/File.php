<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class File extends Model
{

    protected $table      = 'files';
	protected $primaryKey = 'file_id';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_id',
        'post_id',
        'name',
        'path'
    ];


	// Relations
	public function user()
    {
        return $this->hasOne('App\Models\User', 'user_id', 'user_id');
    }
	public function post()
    {
        return $this->hasOne('App\Models\Post', 'post_id', 'post_id');
    }

    
    // Getters
	public function getFileID() {
		return $this->primaryKey;
	}
	public function getUserID() {
		return $this->user_id;
	}
	public function getPostID() {
		return $this->post_id;
	}
	public function getName() {
		return $this->name;
	}
	public function getPath() {
		return $this->path;
	}

}
